package Request_Specification_response_classes;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Get_API_Single_User {


public static void main(String[] args) {
		
		//Step 1 : get all info and store into local variables
		
		String hostname="https://reqres.in/";
		String resource ="api/users/2";
		String headername = "content-type";
		String headervalue = "application/json";
		
		//Step 2: build request specification using requestSpecification class
		
		RequestSpecification req_spac = RestAssured.given();
		
		// Step 2.1 : configure header
		
		req_spac.header(headername,headervalue);
		
		// Step 2.2 : trigger the API fetch response using response class
		
		Response response = req_spac.get(hostname + resource);
		
		// Step 3 : print the response body and fetch,store and print the status code
		
		//System.out.println(response.getBody().asPrettyString());
		
		int status_code = response.statusCode();
		System.out.println(status_code);
		
		// Step 4 : Fetch response body parameters and store into local variables
		
		String res_id = response.getBody().jsonPath().getString("data.id");
		System.out.println(res_id);
		
		String res_email = response.getBody().jsonPath().getString("data.email");
		System.out.println(res_email);
		
		String res_firstName = response.getBody().jsonPath().getString("data.first_name");
		System.out.println(res_firstName);
		
		String res_lastName = response.getBody().jsonPath().getString("data.last_name");
		System.out.println(res_lastName);
		
		String res_url = response.getBody().jsonPath().getString("support.url");
		System.out.println(res_url);
		
		String res_text = response.getBody().jsonPath().getString("support.text");
		System.out.println(res_text);
		
		// Step 5 : Validate status code and response body parameters
		
		Assert.assertEquals(status_code, 200);
		
		Assert.assertEquals(res_id,"2");
		Assert.assertEquals(res_email, "janet.weaver@reqres.in");
		Assert.assertEquals(res_firstName, "Janet");
		Assert.assertEquals(res_lastName, "Weaver");
		Assert.assertEquals(res_url, "https://reqres.in/#support-heading");
		Assert.assertEquals(res_text, "To keep ReqRes free, contributions towards server costs are appreciated!");
		}

}



