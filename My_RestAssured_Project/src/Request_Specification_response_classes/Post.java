package Request_Specification_response_classes;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Post {

	public static void main(String[] args) {
		
		// step 1 : collect all needed information and save into local variables
		
		String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

		String hostname = "https://reqres.in/";

		String resource = "api/users";

		String headername = "Content-Type";

		String headervalue = "application/json";
		
		// step 2 build the request specification by using request specification
		
		RequestSpecification req_spec = RestAssured.given();
		
		// step 2.1 set request header 
		
		req_spec.header(headername, headervalue);
		
		// Step 2.2 set the request body
		
		req_spec.body(req_body);
		
		// step 2.3 = trigger the api request
		
		Response response = req_spec.post(hostname + resource);
		
		System.out.println(response.getBody().asString());
		
		System.out.println(response.statusCode());
		
		System.out.println(response.getBody().jsonPath().getString("name"));
		
		System.out.println(response.getBody().jsonPath().getString("job"));
		
		System.out.println(response.getBody().jsonPath().getString("id"));
		
		System.out.println(response.getBody().jsonPath().getString("CreatedAt"));

	}

}
