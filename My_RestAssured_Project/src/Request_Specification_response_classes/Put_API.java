package Request_Specification_response_classes;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Put_API {

	public static void main(String[] args) {

//		get all info and store into local variable

		String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		String hostname = "https://reqres.in/api/users/2";
		String resource = "/api/users/2";
		String headername = "content-type";
		String headervalue = "application/json";

//		Step 2 : Build the request specification by using request specification class

		RequestSpecification req_spec = RestAssured.given();

		//Step 2.1 :  configure request header 

		req_spec.header(headername, headervalue);

		//Step  2.2 configure request body

		req_spec.body(req_body);

		//Step : 2.3 trigger the request and fetch response body using response class

		Response response = req_spec.put(hostname + resource);

		//Step 3 : print response body and fetch and store status code into local variable

		System.out.println(response.getBody().asPrettyString());

		int status_code = response.statusCode();
		System.out.println(status_code);

		//Step 3.1 : Creating jsonpath object for extracting request body parameters

		JsonPath req_jsn = new JsonPath(req_body);

		String req_name = req_jsn.getString("name");
		System.out.println(req_name);

		String req_job = req_jsn.getString("job");
		System.out.println(req_job);

		System.out.println("-----------------------------");

		//Step 3.2 : Fetch response body parameters store into local variables

		String res_name = response.getBody().jsonPath().getString("name");
		System.out.println(res_name);

		String res_job = response.getBody().jsonPath().getString("job");
		System.out.println(res_job);

		String res_time = response.getBody().jsonPath().getString("updatedAt");
		System.out.println(res_time);
		res_time = res_time.substring(0, 11);
		System.out.println(res_time);

//		Step 4 : convert time stamp into local time 
		LocalDateTime currantdate = LocalDateTime.now();
		String localDate = currantdate.toString().substring(0, 11);

//		Step 5 : Validate status code and responsebody parameters using testng's Assert

		Assert.assertEquals(status_code, 200);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_time, localDate);
	}
}
