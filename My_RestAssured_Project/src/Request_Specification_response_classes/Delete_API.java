package Request_Specification_response_classes;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class Delete_API {

	public static void main(String[] args) {
		
		
		

				// step 1: collect all information for configuring API

				String hostname = "https://reqres.in/";

				String resource = "api/users/2";

				String headername = "Content-Type";

				String headervalue = "application/json";

				// step 2 Build the request specification using request specification class

				RequestSpecification requestSpec = RestAssured.given();

				// step 2.1 set request header

				//requestSpec.header(headername, headervalue);

				// step 2.2 set request body

				// requestSpec.body(req_body);

				// step 3 send api request

				Response response = requestSpec.delete(hostname + resource);

				System.out.println(response.statusCode());

			}
		}
