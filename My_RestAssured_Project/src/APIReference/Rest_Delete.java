package APIReference;

import static io.restassured.RestAssured.given;
import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Rest_Delete {


	public static void main(String[] args) {

		// Step 1 : Collect all needed information and save it into local variables

		String hostname = "https://reqres.in/";

		String resource = "/api/users/2";
		
		// Step 2 : Declare BaseURI 
		
		RestAssured.baseURI=hostname;
		// Step 3 : Configure the API for execution and log entire transaction 
		
        Response res_body = given().when().delete(resource);
	
        //step4:  Extract the status code and the body
        int status = res_body.getStatusCode();
        String body = res_body.getBody().asString();
        // Print the status and the body to the console
        System.out.println("Status: " + status);
        System.out.println(body+ " Deleted");
		
		// Step 5: Validate the response body

		// Step 5.1 : Use TestNG's Assert
      	Assert.assertEquals(status, 204);      
			
	}
}
