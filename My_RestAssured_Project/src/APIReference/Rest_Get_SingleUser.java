package APIReference;

import org.testng.Assert;
import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Rest_Get_SingleUser {

	public static void main(String[] args) {
		// get all input data and store into local variable
		String hostname = "https://reqres.in/api/";
		String resource = "users?page=2";
		String headername = "conent-tpye";
		String headervalue = "json/application";
		
		// declare base uri
		RestAssured.baseURI = hostname;
		// configure api and store response in local variable
		Response res_body = given().header(headername, headervalue).get(resource).then().extract().response();
		String response = res_body.asPrettyString();
		System.out.println(response);
		int status_code = res_body.getStatusCode();
		System.out.println(status_code);

		// validate status code
		
		Assert.assertEquals(status_code, 200);

//         declare json object
		
		JsonPath res_jsn = new JsonPath(response);
		
		// extract array length and array count
		
		int count = res_jsn.getInt("data.size()");
		System.out.println(count);
		int id = 0;
		int idArr[] = new int[count];
		int ids[] = { 7, 8, 9, 10, 11, 12 };
		String emailArr[] = new String[count];

		String emails[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		// String first_names[]= {"Michael","Lindsay", "Tobias","Byron",
		// "George","Rachel"};
		// String last_names[]=
		// {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		// extract response body parameters
		for (int i = 0; i < count; i++) {
			id = res_jsn.getInt("data[" + i + "].id");
			System.out.println(id);
			idArr[i] = id;

			String email = res_jsn.getString("data[" + i + "].email");
			System.out.println(email);
			emailArr[i] = email;

//            	 String first_name=res_jsn.getString("data["+i+"].first_name");
//            	 System.out.println(first_name);
//            	 
//            	 String last_name=res_jsn.getString("data["+i+"].last_name");
//            	 System.out.println(last_name);

		}

		// validate response body parameter id
		for (int i = 0; i < idArr.length; i++) {
			System.out.println(idArr[i]);

			// validate response body parameter email

			Assert.assertEquals(ids[i], idArr[i]);
		}
		for (int i = 0; i < emailArr.length; i++) {
			System.out.println(emailArr[i]);
			Assert.assertEquals(emails[i], emailArr[i]);

		}

	}
}
