package APIReference;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
public class Rest_Post_LoginSuccessful {

	public static void main(String[] args) {
		
		// Step 1 : collect all needed information and save it into local variables
		
		String req_body = "{\r\n"+ "    \"email\": \"eve.holt@reqres.in\",\r\n"+ "    \"password\": \"cityslicka\"\r\n"+ "}";
		
		String hostname = "https://reqres.in/";
				
		String resource = "/api/login";

		String headername =  "Content-Type";
		
		String headervalue = "application/json";
		
		// Step 2 : Declare BaseURI
		
		RestAssured.baseURI = hostname;
		
		// Step 3 : configure the API for execution and log entire transaction(request header, request body, response header, response body, time etc)
		
		/*given().header(headername, headervalue).body(req_body).log().all().post(resource).then().extract().response();*/
		
		// Step 4 : configure the API for execution and save the response in a sting variables
		
		String res_body = given().header(headername, headervalue).body(req_body).when().post(resource).then().extract().response().asString();
		
		System.out.println(req_body);
		
		// Step 5 : Parse the response body
		
		// Step 5.1 : Create the object of jsonPath
		
		JsonPath jsp_res = new JsonPath(res_body);
		
		// step 5.2 : Parse individual params using jsp_res object
		
		String res_token = jsp_res.getString("token");
		System.out.println(res_token);
		
		// Step 6 : Validate the response Body
		
		// Step 6.1 : Parse request body and save into local variables
		
		JsonPath jsp_req = new JsonPath(req_body);
		
		String req_email = jsp_req.getString("email");
		System.out.println(req_email);
		
		String req_password = jsp_req.getString("password");
		System.out.println(req_password);
		
		// Step 6.2 : Use TestNG's Assert
		
		Assert.assertEquals(res_token,  "QpwL5tke4Pnpja7X4");
		
	}

}
