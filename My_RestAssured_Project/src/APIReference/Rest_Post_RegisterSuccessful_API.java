package APIReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;


public class Rest_Post_RegisterSuccessful_API {

	public static void main(String[] args) {
		
		// Step 1 : Collect all needed information and save it into local variables
		
		String req_body = "{\r\n"+ "    \"email\": \"eve.holt@reqres.in\",\r\n"+ "    \"password\": \"pistol\"\r\n"+ "}";
		
		String hostname = "https://reqres.in/";
		
		String resource = "/api/user";
		
		String headername = "Content-type";
		
		String headervalue = "application/json";
		
		// Step 2 : Declare BaseURI
		
		RestAssured.baseURI = hostname;
		
		// Step 3 : Configure the API execution and log entire transaction (request header, request body, response header, response body, time etc) 
		
		/*given().header(headername, headervalue).body(req_body).log().all().post(resource).then().log().all().extract().response();*/
		
		// Step 4 : Configure the API for execution and save the response in a String variable
		
		String res_body=given().header(headername, headervalue).body(req_body).when().post(resource).then().extract().response().asString();
		
		System.out.println(res_body);
		
		// Step 5 : Parse the response body
		
		// Step 5.1 : Create the object of JsonPath
		
		JsonPath jsp_res = new JsonPath(res_body);
		
		// Step 5.2 : Parse individual params using jsp_res object
		
		String res_email = jsp_res.getString("email");
		System.out.println(res_email);
		
		String res_password = jsp_res.getString("password");
		System.out.println(res_password);
		
		String res_id = jsp_res.getString("id");
		System.out.println(res_id);
		
		String res_createdAt = jsp_res.getString("createdAt");
		System.out.println(res_createdAt);
		
		// Step 6 : Validate the response body 
		
		// Step 6.1 : Parse request body and save into local variables
		
		JsonPath jsp_req = new JsonPath(req_body);
		String req_email = jsp_req.getString("email");
		String req_password = jsp_req.getString("password");
		String req_id = jsp_req.getString("id");
		
		// Step 6.2 : Generate expexted date
		
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		System.out.println(expecteddate);
		
		// Step 6.3 : Usr TestNG's Assert 
		
		Assert.assertEquals(res_email, req_email);
		Assert.assertEquals(res_password, req_password);
		Assert.assertNotNull(res_id);
		
		
		
				
				
			

	}

}
