package Repository;

import java.util.ArrayList;
import java.io.IOException;
import Common_Method.Utility;

public class RequestBody extends Environment{
	
	public static String req_post_tc(String TestCaseName) throws IOException {
		ArrayList<String> Data = Utility.readExcelData("Post_API", TestCaseName);
		String key_name = Data.get(1);
		String value_name = Data.get(2);
		String key_job = Data.get(3);
		String value_job = Data.get(4);
		
		String req_body = "{\r\n" + "    \""+key_name+"\": \""+value_name+"\",\r\n" + "    \""+key_job+"\": \""+value_job+"\"\r\n" + "}";
		return req_body;
			
	}
	
	public static String req_put_tc(String TestCaseName) throws IOException {
		ArrayList<String> Data = Utility.readExcelData("Put_API", TestCaseName);
		String key_name = Data.get(1);
		String value_name = Data.get(2);
		String key_job = Data.get(3);
		String value_job = Data.get(4);
		
		String req_body = "{\r\n" + "    \""+key_name+"\": \""+value_name+"\",\r\n" + "    \""+key_job+"\": \""+value_job+"\"\r\n" + "}";
		return req_body;
			
	}
	
	

	
	/*public static String req_tc2() {
		String req_body = "{\r\n" + "    \"name\": \"Vaishnavi\",\r\n" + "    \"job\": \"QA\"\r\n" + "}";
		return req_body;
	}*/
}
	/*
	public static String req_tc3() {
		String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n"
		+ "    \"job\": \"zion resident\"\r\n" + "}";
		return req_body;
		}

		public static String req_tc4() {
		String req_body = "{\r\n" + "    \"email\": \"eve.holt@reqres.in\",\r\n"
		+ "    \"password\": \"pistol\"\r\n"
		+ "}";
		return req_body;
		}*/
	
	
