package Repository;

public class Environment {
	
	public static String Hostname() {
		String hostname = "https://reqres.in/";
		return hostname;
	}

	public static String Resource() {
		String resource = "api/users";
		return resource;
	}

	public static String HeaderName() {

		String headername = "Content-Type";
		return headername;
	}

	public static String HeaderValue() {

		String headervalue = "application/json";
		return headervalue;

	}
	
	/*public static String resource2() {
		String resource2 = "api/users?page=2";
		return resource2;
	}

	public static String resource3() {
		String resource3 = "api/users/2";
		return resource3;
	}

	public static String resource4() {
		String resource4 = "api/register";
		return resource4;
	}

	public static String resource5() {
		String resource5 = "api/users/2";
		return resource5;
	}*/
}
