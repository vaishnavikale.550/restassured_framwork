package Common_Method;

import Repository.RequestBody;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class API_Trigger  {

	public static Response post_trigger(String HeaderName, String HeaderValue, String Reqbody,
			String Endpoint) {

		//  Build the request specification using RequestSpecification class

		RequestSpecification requestSpec = RestAssured.given();

		//  Set request header

		requestSpec.header(HeaderName, HeaderValue);

		//  Set request body

		requestSpec.body(Reqbody);

		//  Send the API request

		Response response = requestSpec.post(Endpoint);
		return response;
	}


}
