Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Wed, 28 Feb 2024 08:20:51 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"969","createdAt":"2024-02-28T08:20:51.857Z"}